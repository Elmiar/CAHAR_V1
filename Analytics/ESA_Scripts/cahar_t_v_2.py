import numpy as np
import os
import pickle
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable
import sklearn
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn import linear_model
from math import sqrt
import warnings


df1, df2 = pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user1.features_labels.csv"), pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user2.features_labels.csv")

#print(df1.describe(), df2.describe())
#print(df1.shape)
#print(df2.shape)

#print(df1.info(verbose=True))
#print(df2.info(verbose=True))

#print(len(df1.columns))
#print(len(df2.columns))

context_labels_1, context_labels_2 = [col for col in df1 if col.startswith('label:')], [col for col in df2 if col.startswith('label:')]

labels1, labels2 = []*0, []*0

for i in context_labels_1:
    
    labels1.append(i[6:])

for i in context_labels_2:
    
    labels2.append(i[6:])

#print(context_labels_1, context_labels_2)

ft_labels_1, ft_labels_2 = [col for col in df1 if not col.startswith('label') and not col.startswith('timestamp')], [col for col in df2 if not col.startswith('label') and not col.startswith('timestamp')]

#print(len(ft_labels_1), len(ft_labels_2))

scores = []*0
#cl_rep = []*0
#for i in context_labels_1:
    
X, Y = df2, df2[context_labels_1]

#Z = df2

X[np.isnan(X)] = 0.

Y[np.isnan(Y)] = 0.

#Z[np.isnan(Z)] = 0.
    
X_train, X_test, Y_train, Y_test = tts(X, Y, test_size=0.20)

    #print(X_train.shape, X_test.shape, Y_train.shape, Y_test.shape)
        
#clf = sklearn.tree.DecisionTreeRegressor()

#parameters = {
#    "n_estimators":[5,10,50,100,250],
#    "max_depth":[2,4,8,16,32,None]
    
#}

#from sklearn.model_selection import GridSearchCV
#cv = GridSearchCV(clf,parameters,cv=5)

#(train_features,train_label.values.ravel())

#h = clf.fit(X_train, Y_train)

with open('/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/models/trained_on_user_2/regressor_complete_feature_set.pkl', 'rb') as f:

    clf = pickle.load(f)


y_prediction_rf = clf.predict(X_test)

#print(Y_test)

og, pv = []*0, []*0

for index, row in X_test[context_labels_1].iterrows():

    og.append(row)

og = np.array(og)

for j in y_prediction_rf:

    pv.append(j)

pv = np.array(pv)

res = np.equal(pv, og)

print(X_test.shape)
print(pv)
print(og)
for i in range(X_test.shape[0]):
    print(X_test[i], ' = ', y_prediction_rf[i])
    input
    
'''
for (i, j) in zip(Y_test, y_prediction_rf):

    print(i)
    print()
    ind = j.where(j==1)
    print(ind[0])
    if(len(ind[0])!=None):
        for i in (ind[0]):
            
            #pred_labs = [context_labels_1[ind[0, 0]], context_labels_1[ind[0, 1]]]
            print(context_labels_1[i])
            print()
        #print(i in pred_labs)
        input()

print(accuracy_score(Y_test, y_prediction_rf))

print(classification_report(Y_test, y_prediction_rf))

    
    #cl_rep.append(classification_report(X_train, y_prediction_rf))
    
print("finished")
'''
