Testing Started at : 2021-08-31 21:52:18.169623
CSV loaded in: 0:00:00.012226
Data loaded and pre-processed in: 0:00:49.616312
<class 'pandas.core.frame.DataFrame'>
Int64Index: 1244 entries, 0 to 1243
Data columns (total 27 columns):
 #   Column               Non-Null Count  Dtype  
---  ------               --------------  -----  
 0   timestamp            1244 non-null   float64
 1    xValue              1244 non-null   float64
 2    yValue              1244 non-null   float64
 3    zValue              1244 non-null   float64
 4    xGyroValue          1244 non-null   float64
 5    yGyroValue          1244 non-null   float64
 6    zGyroValue          1244 non-null   float64
 7    xMagValue           1244 non-null   float64
 8    yMagValue           1244 non-null   float64
 9    zMagValue           1244 non-null   float64
 10   lin_accxValue       1244 non-null   float64
 11   lin_accyValue       1244 non-null   float64
 12   lin_acczValue       1244 non-null   float64
 13   xGyroUncalibValue   1244 non-null   float64
 14   yGyroUncalibValue   1244 non-null   float64
 15   zGyroUncalibValue   1244 non-null   float64
 16   xMagnoUncalibValue  1244 non-null   float64
 17   yMagnoUncalibValue  1244 non-null   float64
 18   zMagnoUncalibValue  1244 non-null   float64
 19   Rotvecx             1244 non-null   float64
 20   Rotvecy             1244 non-null   float64
 21   Rotvecz             1244 non-null   float64
 22  Roll                 1244 non-null   float64
 23   Pitch               1244 non-null   float64
 24   Yaw                 1244 non-null   float64
 25  Magnitude            1244 non-null   float64
 26  AbsDif               1244 non-null   float64
dtypes: float64(27)
memory usage: 272.1 KB
None
Index(['timestamp', ' xValue', ' yValue', ' zValue', ' xGyroValue',
       ' yGyroValue', ' zGyroValue', ' xMagValue', ' yMagValue', ' zMagValue',
       ' lin_accxValue', ' lin_accyValue', ' lin_acczValue',
       ' xGyroUncalibValue', ' yGyroUncalibValue', ' zGyroUncalibValue',
       ' xMagnoUncalibValue', ' yMagnoUncalibValue', ' zMagnoUncalibValue',
       ' Rotvecx', ' Rotvecy', ' Rotvecz', 'Roll', ' Pitch', ' Yaw',
       'Magnitude', 'AbsDif'],
      dtype='object')
(1244, 27)
Index(['Roll', ' Pitch', ' Yaw', ' xGyroValue', ' yGyroValue', ' zGyroValue',
       ' Rotvecx', ' Rotvecy', ' Rotvecz', ' lin_accxValue', ' lin_accyValue',
       ' lin_acczValue', ' xValue', ' yValue', ' zValue'],
      dtype='object')
Predicted in: 0:00:49.683154
[1.9375 1.9375 1.9375 ... 1.9375 1.9375 1.9375]
