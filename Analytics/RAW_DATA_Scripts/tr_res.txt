Training Started at : 2021-08-31 20:40:15.264647
CSV loaded in: 0:00:00.746301
NaN filled in: 0:00:00.795642
Type changed in: 0:00:00.840360
Type changed in: 0:00:00.882385
Label encoded in: 0:00:00.928911
Label encoded in: 0:00:00.977416
(365225, 15)
Data Processed in: 0:00:00.992345
Index(['attitude.roll', 'attitude.pitch', 'attitude.yaw', 'rotationRate.x',
       'rotationRate.y', 'rotationRate.z', 'userAcceleration.x',
       'userAcceleration.y', 'userAcceleration.z', 'x_accel', 'y_accel',
       'z_accel', 'x_gyro', 'y_gyro', 'z_gyro'],
      dtype='object')
TTS in: 0:00:01.032826
(292180, 15) (73045, 15) (292180,) (73045,)
Regressor Trained in: 0:00:16.135904
Predicted in: 0:00:16.241005
Target
 92426     3
185205    4
128825    3
40509     1
106700    3
         ..
79235     2
1862      1
339357    6
221637    4
70658     2
Name: label, Length: 73045, dtype: int64
Prediction
 [3. 4. 3. ... 6. 4. 1.]
precent
[88.8958861]
