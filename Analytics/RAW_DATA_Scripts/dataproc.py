import pandas as pd
import csv, random
import numpy as np
import pickle
import numpy as np
import os
import pickle
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable
import sklearn
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn import linear_model
from math import sqrt
from statsmodels.nonparametric.kde import KDEUnivariate
import warnings
from sklearn import preprocessing
from datetime import datetime as dt

'''
DAT = []



f = open('output.csv', 'r')
dat = csv.reader(f)
r = 0
for row in dat:
    r+=1
    ROW = row[0].split(',')
    #print(ROW)

    while '' in ROW:
        ROW.remove('')

    for i in ROW[1:]:
        ind = ROW.index(i)
        i = i.split(':')
        ROW[ind] = i[-1]
            
    for i in range(len(ROW)):
        ROW[i] = float(ROW[i])
    if(len(ROW)>21):        
        DAT.append(ROW)

#print(len(DAT), r)

DATA = np.asarray(DAT)

#print(DATA)

cols = "timestamp, xValue, yValue, zValue, xGyroValue, yGyroValue, zGyroValue, xMagValue, yMagValue, zMagValue, lin_accxValue, lin_accyValue, lin_acczValue, xGyroUncalibValue, yGyroUncalibValue, zGyroUncalibValue, xMagnoUncalibValue, yMagnoUncalibValue, zMagnoUncalibValue, Rotvecx, Rotvecy, Rotvecz"
cols = cols.split(',')
#print(len(cols))
df = pd.DataFrame(data=DAT,    # values
              index=[i for i in range(DATA.shape[0])],    #  column as index
              columns=cols)  # the column names

df1 = df[cols[:4]].copy()

Roll, Pitch, Yaw, Magnitude = []*0, []*0, []*0, []*0

for i in range(df1.shape[0]):

    Roll.append(df1.iloc[i, 1]/((df1.iloc[i, 2]**(2) + df1.iloc[i, 3]**(2))**(0.5)))
    Pitch.append(df1.iloc[i, 2]/((df1.iloc[i, 1]**(2) + df1.iloc[i, 3]**(2))**(0.5)))
    Yaw.append(df1.iloc[i, 3]/((df1.iloc[i, 1]**(2) + df1.iloc[i, 2]**(2))**(0.5)))
    Magnitude.append((df1.iloc[i, 1]**(2) + df1.iloc[i, 2]**(2) + df1.iloc[i, 3]**(2))**(0.5))
    Absolute_Diff = [i - sum(Magnitude)//len(Magnitude) for i in Magnitude]

df1['Roll'] = Roll
df1['Pitch'] = Pitch
df1['Yaw'] = Yaw
df1['Magnitude'] = Magnitude
df1['AbsDif'] = Absolute_Diff


Max_Value = [df1[cols[1]].max(), df1[cols[2]].max(), df1[cols[3]].max()]
print("Maimum = ", Max_Value) 
#df1['Centroid_x'], df1['Centroid_y'], df1['Centroid_z'] = df1[cols[1]].mean(), df1[cols[2]].mean(), df1[cols[3]].mean()
Cent = df1[cols[1]].mean(), df1[cols[2]].mean(), df1[cols[3]].mean()
print("Centroid = ", Cent) 
#df1['SD_x_acc'], df1['SD_y_acc'], df1['SD_z_acc'] = df1[cols[1]].std(skipna=True), df1[cols[2]].std(skipna=True), df1[cols[3]].std(skipna=True)
SD = [df1[cols[1]].std(skipna=True), df1[cols[2]].std(skipna=True), df1[cols[3]].std(skipna=True)]
print("Standard Deviation = ", SD) 
Density_x, Density_y, Density_z = KDEUnivariate(df1[cols[1]]).fit().density, KDEUnivariate(df1[cols[2]]).fit().density, KDEUnivariate(df1[cols[3]]).fit().density
print("Density = ", Density_x, Density_y, Density_z) 
'''
'''
print(df.info(verbose=True))
print(df)
print(df.columns)
print(df.shape)
'''
'''
colsn = df1.columns
for i in colsn[1:]:
    plt.plot([i for i in range(len(df1[i]))], df1[i])
    plt.title(i)
    plt.savefig("{}".format(i))
    plt.show()
'''
'''



labels = "Walking, Jogging, Sitting, Standing, Upstairs, Downstairs".split(',')
labels = [random.choice(labels) for i in range(df1.shape[0])]
label_encoder = LabelEncoder()
labels_encoded = label_encoder.fit_transform(labels)

#print(df1)

min_max_scaler = preprocessing.MinMaxScaler()
scaled = min_max_scaler.fit_transform(df1)
df1 = pd.DataFrame(scaled)

#df1['labels'] = labels_encoded

print(df1.columns)

#dflabels = df1.labels
#print(df1)
'''
st = dt.now()
print(st)
label_encoder = LabelEncoder()

X = pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/Grand_Data.csv", low_memory=False)

et = dt.now()
print(et-st)

#X = X.T.drop_duplicates().T
#et = dt.now()
#print(et-st)

X = X.fillna(0)
et = dt.now()
print(et-st)

X['user'] = X.user.astype(str)
et = dt.now()
print(et-st)

X['label'] = X.label.astype(str)
et = dt.now()
print(et-st)

X = X.apply(label_encoder.fit_transform(X['user']))
et = dt.now()
print(et-st)

X = X.apply(label_encoder.fit_transform(X['label']))
et = dt.now()
print(et-st)

Train_labels = X['label']

et = dt.now()
print(et-st)
print(X.columns)

#labels_encoded = label_encoder.fit_transform(Train_labels)

#min_max_scaler = preprocessing.MinMaxScaler()
#X = min_max_scaler.fit_transform(X)

#et = dt.now()
#print(et-st)


X_train, X_test, Y_train, Y_test = tts(X, Train_labels, test_size=0.20)
et = dt.now()
print(et-st)

print(X_train, X_test, Y_train, Y_test)

reg = sklearn.tree.DecisionTreeRegressor()

#Y_train = [random.choices([1, 2], weights=[0.8, 0.2])[0] for i in range(X_train.shape[0])]
#Y_test = [random.choices([1, 2], weights=[0.5, 0.5])[0] for i in range(X_test.shape[0])]

h = reg.fit(X_train, Y_train)
et = dt.now()
print(et-st)

pickle.dump(h, "./GDRData.pkl")
res = reg.predict(X_test)
et = dt.now()
print(et-st)

chk = np.equal(Y_test, res)
print(chk)
print(np.sum(chk), chk.shape)
