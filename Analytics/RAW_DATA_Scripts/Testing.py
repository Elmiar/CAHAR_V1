import pandas as pd
import csv, random
import numpy as np
import pickle
import numpy as np
import os
import pickle
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable
import sklearn
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn import linear_model
from math import sqrt
from statsmodels.nonparametric.kde import KDEUnivariate
import warnings
from sklearn import preprocessing
from datetime import datetime as dt

'''
Index(['Unnamed: 0', 'Unnamed: 0.1', 'attitude.roll', 'attitude.pitch',
       'attitude.yaw', 'gravity.x', 'gravity.y', 'gravity.z', 'rotationRate.x',
       'rotationRate.y', 'rotationRate.z', 'userAcceleration.x',
       'userAcceleration.y', 'userAcceleration.z', 'x_accel', 'y_accel',
       'z_accel', 'x_gyro', 'y_gyro', 'z_gyro', 'user', 'label'],
      dtype='object')

timestamp,
xValue, yValue, zValue,
xGyroValue, yGyroValue, zGyroValue,
xMagValue, yMagValue, zMagValue,
lin_accxValue, lin_accyValue, lin_acczValue,
xGyroUncalibValue, yGyroUncalibValue, zGyroUncalibValue,
xMagnoUncalibValue, yMagnoUncalibValue, zMagnoUncalibValue,
Rotvecx, Rotvecy, Rotvecz,
Roll, Pitch, Yaw,
Magnitude, AbsoluteDifference


common features

'attitude.roll',        Roll, 
'attitude.pitch',       Pitch, 
'attitude.yaw',         Yaw,
'x_gyro',               xGyroValue,
'y_gyro',               yGyroValue,
'z_gyro',               zGyroValue,
'rotationRate.x',       Rotvecx
'rotationRate.y',       Rotvecy
'rotationRate.z',       Rotvecz
'userAcceleration.x',   lin_accxValue,
'userAcceleration.y',   lin_accyValue,
'userAcceleration.z',   lin_acczValue,
'x_accel',              xValue,
'y_accel',              yValue,
'z_accel',              zValue,


Trained on (Features)

['attitude.roll', 'attitude.pitch', 'attitude.yaw', 'rotationRate.x', 'rotationRate.y', 'rotationRate.z', 'userAcceleration.x', 'userAcceleration.y', 'userAcceleration.z', 'x_accel', 'y_accel', 'z_accel', 'x_gyro', 'y_gyro', 'z_gyro', 'label']

Testing must be made on (Features)

"Roll, Pitch, Yaw, xGyroValue, yGyroValue, zGyroValue, Rotvecx, Rotvecy, Rotvecz, lin_accxValue, lin_accyValue, lin_acczValue, xValue, yValue, zValue"

'''
st = dt.now()
print("Testing Started at :", st)

DAT = []

TestingFeatures = "Roll, Pitch, Yaw, xGyroValue, yGyroValue, zGyroValue, Rotvecx, Rotvecy, Rotvecz, lin_accxValue, lin_accyValue, lin_acczValue, xValue, yValue, zValue"
Te_ft = TestingFeatures.split(',')

f = open('/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/C.A.H.A.R/Analytics/RAW_DATA_Scripts/App_Output/output.csv', 'r')
dat = csv.reader(f)
et = dt.now()
print("CSV loaded in:", et-st)


r = 0
for row in dat:
    r+=1
    ROW = row[0].split(',')
    #print(ROW)

    while '' in ROW:
        ROW.remove('')

    for i in ROW[1:]:
        ind = ROW.index(i)
        i = i.split(':')
        ROW[ind] = i[-1]
            
    for i in range(len(ROW)):
        ROW[i] = float(ROW[i])
    if(len(ROW)>21):        
        DAT.append(ROW)

#print(len(DAT), r)

DATA = np.asarray(DAT)

#print(DATA)

cols = "timestamp, xValue, yValue, zValue, xGyroValue, yGyroValue, zGyroValue, xMagValue, yMagValue, zMagValue, lin_accxValue, lin_accyValue, lin_acczValue, xGyroUncalibValue, yGyroUncalibValue, zGyroUncalibValue, xMagnoUncalibValue, yMagnoUncalibValue, zMagnoUncalibValue, Rotvecx, Rotvecy, Rotvecz"
cols = cols.split(',')
#print(len(cols))
df = pd.DataFrame(data=DAT,    # values
              index=[i for i in range(DATA.shape[0])],    #  column as index
              columns=cols)  # the column names

df1 = df.copy()

Roll, Pitch, Yaw, Magnitude = []*0, []*0, []*0, []*0

for i in range(df1.shape[0]):

    Roll.append(df1.iloc[i, 1]/((df1.iloc[i, 2]**(2) + df1.iloc[i, 3]**(2))**(0.5)))
    Pitch.append(df1.iloc[i, 2]/((df1.iloc[i, 1]**(2) + df1.iloc[i, 3]**(2))**(0.5)))
    Yaw.append(df1.iloc[i, 3]/((df1.iloc[i, 1]**(2) + df1.iloc[i, 2]**(2))**(0.5)))
    Magnitude.append((df1.iloc[i, 1]**(2) + df1.iloc[i, 2]**(2) + df1.iloc[i, 3]**(2))**(0.5))
    Absolute_Diff = [i - sum(Magnitude)//len(Magnitude) for i in Magnitude]

df1['Roll'] = Roll
df1[' Pitch'] = Pitch
df1[' Yaw'] = Yaw
df1['Magnitude'] = Magnitude
df1['AbsDif'] = Absolute_Diff


Max_Value = [df1[cols[1]].max(), df1[cols[2]].max(), df1[cols[3]].max()]
#print("Maimum = ", Max_Value) 
#df1['Centroid_x'], df1['Centroid_y'], df1['Centroid_z'] = df1[cols[1]].mean(), df1[cols[2]].mean(), df1[cols[3]].mean()
Cent = df1[cols[1]].mean(), df1[cols[2]].mean(), df1[cols[3]].mean()
#print("Centroid = ", Cent) 
#df1['SD_x_acc'], df1['SD_y_acc'], df1['SD_z_acc'] = df1[cols[1]].std(skipna=True), df1[cols[2]].std(skipna=True), df1[cols[3]].std(skipna=True)
SD = [df1[cols[1]].std(skipna=True), df1[cols[2]].std(skipna=True), df1[cols[3]].std(skipna=True)]
#print("Standard Deviation = ", SD) 
Density_x, Density_y, Density_z = KDEUnivariate(df1[cols[1]]).fit().density, KDEUnivariate(df1[cols[2]]).fit().density, KDEUnivariate(df1[cols[3]]).fit().density
#print("Density = ", Density_x, Density_y, Density_z) 

et = dt.now()
print("Data loaded and pre-processed in:", et-st)


print(df1.info(verbose=True))
#print(df)
print(df1.columns)
print(df1.shape)
'''
colsn = df1.columns
for i in colsn[1:]:
    plt.plot([i for i in range(len(df1[i]))], df1[i])
    plt.title(i)
    plt.savefig("{}".format(i))
    plt.show()
'''

'''
labels = "Walking, Jogging, Sitting, Standing, Upstairs, Downstairs".split(',')
labels = [random.choice(labels) for i in range(df1.shape[0])]
label_encoder = LabelEncoder()
labels_encoded = label_encoder.fit_transform(labels)
'''
#print(df1)
'''
min_max_scaler = preprocessing.MinMaxScaler()
scaled = min_max_scaler.fit_transform(df1)
df1 = pd.DataFrame(scaled)
'''
#df1['labels'] = labels_encoded

X_test = df1[Te_ft]
print(X_test.columns)

#dflabels = df1.labels
#print(df1)

#label_encoder = LabelEncoder()

#pickle.dump(h, "./GDRData.pkl")

with open("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/C.A.H.A.R/Analytics/RAW_DATA_Scripts/GDRFRData.pkl", 'rb') as f:
    reg = pickle.load(f)
    
res = reg.predict(X_test)
et = dt.now()
print("Predicted in:", et-st)
print(res)
