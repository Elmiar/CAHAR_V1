import pandas as pd
import csv, random
import numpy as np
import pickle
import numpy as np
import os
import pickle
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable
import sklearn
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn import linear_model
from math import sqrt
from statsmodels.nonparametric.kde import KDEUnivariate
import warnings
from sklearn import preprocessing
from datetime import datetime as dt

st = dt.now()
print("Training Started at :", st)
label_encoder = LabelEncoder()

X = pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/C.A.H.A.R/Analytics/RAW_DATA_Scripts/Grand_Data.csv", low_memory=False)

et = dt.now()
print("CSV loaded in:", et-st)

#X = X.T.drop_duplicates().T
#et = dt.now()
#print(et-st)

X = X.fillna(0)
et = dt.now()
print("NaN filled in:", et-st)

X['user'] = X.user.astype(str)
et = dt.now()
print("Type changed in:", et-st)

X['label'] = X.label.astype(str)
et = dt.now()
print("Type changed in:", et-st)

X['user'] = label_encoder.fit_transform(X['user'])
et = dt.now()
print("Label encoded in:", et-st)

print(sorted(set(X['label'])))
print(sorted(set(label_encoder.fit_transform(X['label']))))

X['label'] = label_encoder.fit_transform(X['label'])
et = dt.now()
print("Label encoded in:", et-st)

Train_labels = X['label']

Tr_ft = ['attitude.roll', 'attitude.pitch', 'attitude.yaw', 'rotationRate.x', 'rotationRate.y', 'rotationRate.z', 'userAcceleration.x', 'userAcceleration.y', 'userAcceleration.z', 'x_accel', 'y_accel', 'z_accel', 'x_gyro', 'y_gyro', 'z_gyro']

X = X[Tr_ft]
print(X.shape)

et = dt.now()
print("Data Processed in:", et-st)
print(X.columns)

X_train, X_test, Y_train, Y_test = tts(X, Train_labels, test_size=0.20)
et = dt.now()
print("TTS in:", et-st)

print(X_train.shape, X_test.shape, Y_train.shape, Y_test.shape)

reg = RandomForestRegressor(n_estimators = 16, random_state = 0)

#Y_train = [random.choices([1, 2], weights=[0.8, 0.2])[0] for i in range(X_train.shape[0])]
#Y_test = [random.choices([1, 2], weights=[0.5, 0.5])[0] for i in range(X_test.shape[0])]

h = reg.fit(X_train, Y_train)

et = dt.now()
print("Regressor Trained in:", et-st)
with open("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/C.A.H.A.R/Analytics/RAW_DATA_Scripts/GDRFRData.pkl", 'wb') as f:
    pickle.dump(h, f)
f.close()
res = reg.predict(X_test)
et = dt.now()
print("Predicted in:", et-st)
print("Target\n", Y_test)
print("Prediction\n", res)
chk = np.equal(Y_test, res)
#for c in chk:
#    print(c, end=', ')
print("precent")
print((np.sum(chk)/chk.shape)*100)
