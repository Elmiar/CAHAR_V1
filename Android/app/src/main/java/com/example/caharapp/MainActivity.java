package com.example.caharapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


/*
* These sensors include smartphone motion sensors, such as,
    * accelerometer,
    * gyroscope, and
    * magnetometer,
* audio sensor,
    * microphone,
* phone GPS,
* watch accelerometer (with a sampling rate of 25 Hz), and
* watch compass.
* In our proposed CAHAR scheme, we only used raw
data from the accelerometer sensor of the smartphone, which
was collected at a sampling rate of 40 Hz.*/

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private static final String TAG = "MainActivity";
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private final ArrayList<String> row = new ArrayList<>();

    TextView xValue, yValue, zValue, xGyroValue, yGyroValue, zGyroValue, xMagValue, yMagValue, zMagValue, lin_accxValue, lin_accyValue, lin_acczValue, xGyroUncalibValue, yGyroUncalibValue, zGyroUncalibValue, xMagnoUncalibValue, yMagnoUncalibValue, zMagnoUncalibValue, Rotvecx, Rotvecy, Rotvecz;

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissions() {
        int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
        Toast.makeText(getBaseContext(), "Permission is already granted", Toast.LENGTH_LONG).show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
                Toast.makeText(getBaseContext(), "Permission Granted", Toast.LENGTH_LONG).show();
            } else {
                // Permission Denied
                Toast.makeText(getBaseContext(), "WRITE_EXTERNAL_STORAGE Denied", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SensorManager sensorManager;
        Sensor accelerometer, mGyro, mMagno, lin_acc, mGyroUncalib, mMagnoUncalib, Rotvec;



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();

        xValue = findViewById(R.id.xValue);
        yValue = findViewById(R.id.yValue);
        zValue = findViewById(R.id.zValue);

        xGyroValue = findViewById(R.id.xGyroValue);
        yGyroValue = findViewById(R.id.yGyroValue);
        zGyroValue = findViewById(R.id.zGyroValue);

        xMagValue = findViewById(R.id.xMagValue);
        yMagValue = findViewById(R.id.yMagValue);
        zMagValue = findViewById(R.id.zMagValue);

        lin_accxValue = findViewById(R.id.lin_accxValue);
        lin_accyValue = findViewById(R.id.lin_accyValue);
        lin_acczValue = findViewById(R.id.lin_acczValue);

        xGyroUncalibValue = findViewById(R.id.xGyroUncalibValue);
        yGyroUncalibValue = findViewById(R.id.yGyroUncalibValue);
        zGyroUncalibValue = findViewById(R.id.zGyroUncalibValue);

        xMagnoUncalibValue = findViewById(R.id.xMagnoUncalibValue);
        yMagnoUncalibValue = findViewById(R.id.yMagnoUncalibValue);
        zMagnoUncalibValue = findViewById(R.id.zMagnoUncalibValue);

        Rotvecx = findViewById(R.id.Rotvecx);
        Rotvecy = findViewById(R.id.Rotvecy);
        Rotvecz = findViewById(R.id.Rotvecz);


        Log.d(TAG, "onCreate: Initializing Sensors");
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(MainActivity.this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        mGyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorManager.registerListener(MainActivity.this, mGyro, SensorManager.SENSOR_DELAY_NORMAL);

        mMagno = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorManager.registerListener(MainActivity.this, mMagno, SensorManager.SENSOR_DELAY_NORMAL);

        lin_acc = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(MainActivity.this, lin_acc, SensorManager.SENSOR_DELAY_NORMAL);

        mGyroUncalib = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
        sensorManager.registerListener(MainActivity.this, mGyroUncalib, SensorManager.SENSOR_DELAY_NORMAL);

        mMagnoUncalib = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        sensorManager.registerListener(MainActivity.this, mMagnoUncalib, SensorManager.SENSOR_DELAY_NORMAL);

        Rotvec = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        sensorManager.registerListener(MainActivity.this, Rotvec, SensorManager.SENSOR_DELAY_NORMAL);

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Log.d(TAG, "onSensorChanged:" + "Timestamp:    " + sensorEvent.timestamp + "Acc:    X: " + sensorEvent.values[0] + "Acc:    Y: " + sensorEvent.values[1] + "Acc:    Z: " + sensorEvent.values[2] + "Gyro:    X: " + sensorEvent.values[0] + "Gyro:    Y: " + sensorEvent.values[1] + "Gyro:    Z: " + sensorEvent.values[2] + "Mag:    X: " + sensorEvent.values[0] + "Mag:    Y: " + sensorEvent.values[1] + "Mag:    Z: " + sensorEvent.values[2]);
        Sensor sensor = sensorEvent.sensor;

        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            xValue.setText("Acc : xValue : " + sensorEvent.values[0]);
            yValue.setText("Acc : yValue : " + sensorEvent.values[1]);
            zValue.setText("Acc : zValue : " + sensorEvent.values[2]);
        }

        if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            xGyroValue.setText("Gyro : xValue : " + sensorEvent.values[0]);
            yGyroValue.setText("Gyro : yValue : " + sensorEvent.values[1]);
            zGyroValue.setText("Gyro : zValue : " + sensorEvent.values[2]);
        }

        if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            xMagValue.setText("Mag : xValue : " + sensorEvent.values[0]);
            yMagValue.setText("Mag : yValue : " + sensorEvent.values[1]);
            zMagValue.setText("Mag : zValue : " + sensorEvent.values[2]);
        }

        if (sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            lin_accxValue.setText("Lin_Acc : xValue : " + sensorEvent.values[0]);
            lin_accyValue.setText("Lin_Acc : yValue : " + sensorEvent.values[1]);
            lin_acczValue.setText("Lin_Acc : zValue : " + sensorEvent.values[2]);
        }

        if (sensor.getType() == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
            xGyroUncalibValue.setText("Unc_Gyro : xValue : " + sensorEvent.values[0]);
            yGyroUncalibValue.setText("Unc_Gyro : yValue : " + sensorEvent.values[1]);
            zGyroUncalibValue.setText("Unc_Gyro : zValue : " + sensorEvent.values[2]);
        }

        if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
            xMagnoUncalibValue.setText("Unc_Magno : xValue : " + sensorEvent.values[0]);
            yMagnoUncalibValue.setText("Unc_Magno : yValue : " + sensorEvent.values[1]);
            zMagnoUncalibValue.setText("Unc_Magno : zValue : " + sensorEvent.values[2]);
        }

        if (sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            Rotvecx.setText("Rot : xValue : " + sensorEvent.values[0]);
            Rotvecy.setText("Rot : yValue : " + sensorEvent.values[1]);
            Rotvecz.setText("Rot : zValue : " + sensorEvent.values[2]);
        }

        String col = "";//new ArrayList<>();

        col += (sensorEvent.timestamp);
        col +=',';
        col += (xValue.getText().toString());
        col +=',';
        col += (yValue.getText().toString());
        col +=',';
        col += (zValue.getText().toString());
        col +=',';
        col += (xGyroValue.getText().toString());
        col +=',';
        col += (yGyroValue.getText().toString());
        col +=',';
        col += (zGyroValue.getText().toString());
        col +=',';
        col += (xMagValue.getText().toString());
        col +=',';
        col += (yMagValue.getText().toString());
        col +=',';
        col += (zMagValue.getText().toString());

        col +=',';
        col += (lin_accxValue.getText().toString());
        col +=',';
        col += (lin_accyValue.getText().toString());
        col +=',';
        col += (lin_acczValue.getText().toString());

        col +=',';
        col += (xGyroUncalibValue.getText().toString());
        col +=',';
        col += (yGyroUncalibValue.getText().toString());
        col +=',';
        col += (zGyroUncalibValue.getText().toString());

        col +=',';
        col += (xMagnoUncalibValue.getText().toString());
        col +=',';
        col += (yMagnoUncalibValue.getText().toString());
        col +=',';
        col += (zMagnoUncalibValue.getText().toString());

        col +=',';
        col += (Rotvecx.getText().toString());
        col +=',';
        col += (Rotvecy.getText().toString());
        col +=',';
        col += (Rotvecz.getText().toString());
        row.add(col);

        System.out.println(row.size());

        File Exter = Environment.getExternalStorageDirectory();
        String Path = Exter.getAbsolutePath();
        System.out.println(Path);
        File dir = new File(Exter.getAbsolutePath() + "/cahar");
        Boolean dirsMade = dir.mkdir();
        System.out.println(dirsMade);
        Log.v("Accel", dirsMade.toString());

        File file = new File(dir, "output.csv");
        FileWriter  Fwriter = null;
        try {
            Fwriter = new FileWriter(file);

        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVWriter writer = new CSVWriter(Fwriter);


        try {
            for( int i=0; i < row.size(); i++) {
                String[] data = new String[]{row.get(i)};
                writer.writeNext(data);//(new String[]{data});
                //writer.close();
            }
            writer.close();
            Toast.makeText(getBaseContext(), "Data saved", Toast.LENGTH_LONG).show();
        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }


    }

}

